/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.exhausivesearch;

/**
 *
 * @author focus
 */
public class Exhaustivesearch {

    public static void main(String[] args) {
        int[] a = {10, 3, 5, 7, 11, 1, 17};
        showInput(a);
        Ehts exs = new Ehts(a);
        exs.process();
        exs.sum(); //sum and show result

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }
//Finish
}
