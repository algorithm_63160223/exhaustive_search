/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sukdituch.exhausivesearch;

import java.util.ArrayList;

/**
 *
 * @author focus
 */
public class Ehts {

        private int[] input;
    static int indexArr = 0;
    static ArrayList<ArrayList<Integer>> pair1 = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> pair2 = new ArrayList<ArrayList<Integer>>();

    public Ehts(int[] input) {
        this.input = input;
    }

    public void process() {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<Integer> temp2 = new ArrayList<>();

        for (int o = 0; o < input.length; o++) {
            temp.add(input[o]);
        }
        for (int i = 0; i < input.length; i++) {
            temp2.add(input[i]);
        }

        int arr[] = input;
        int n = arr.length;

        for (int i = input.length; i >= 0; i--) {
            prepareArray(arr, n, i);
        }

        indexArr = 1;
        pair2.add(0, temp2);
        for (int j = 1; j < pair1.size(); j++) {
            temp = new ArrayList<>();
            for (int i = 0; i < input.length; i++) {
                temp.add(input[i]);
            }
            for (int k = 0; k < pair1.get(j).size(); k++) {
                for (int u = 0; u < temp.size(); u++) {
                    if (temp.get(u) == pair1.get(j).get(k)) {
                        temp.remove(u);
                        u--;
                    }
                }
            }
            pair2.add(indexArr, temp);
            indexArr++;
        }
    }

    static void prepareArray(int arr[], int n, int r) {
        int data[] = new int[r];
        combinationUtil(arr, data, 0, n - 1, 0, r);
    }

    static void combinationUtil(int arr[], int data[], int start,
            int end, int index, int r) {
        ArrayList<Integer> temp = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                temp.add(data[j]);
            }
            pair1.add(indexArr, temp);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            combinationUtil(arr, data, i + 1, end, index + 1, r);
        }
    }

    public void getDisjointArray1() {
        for (int i = 0; i < pair1.size(); i++) {
            for (int j = 0; j < pair1.get(i).size(); j++) {
                System.out.print(pair1.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void getDisjointArray2() {
        for (int i = 0; i < pair2.size(); i++) {
            for (int j = 0; j < pair2.get(i).size(); j++) {
                System.out.print(pair2.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void sum() {
        for (int i = 0; i < pair1.size(); i++) {
            if (pair1.contains(pair2.get(i))) {
                pair2.remove(i);
                pair1.remove(i);
            }
        }
        for (int i = 0; i < pair1.size(); i++) {
            int sumA = 0;
            int sumB = 0;
            for (int j = 0; j < pair1.get(i).size(); j++) {
                sumA += pair1.get(i).get(j);
            }
            for (int j = 0; j < pair2.get(i).size(); j++) {
                sumB += pair2.get(i).get(j);
            }
            if (sumA == sumB) {
                System.out.println(pair1.get(i) + " " + pair2.get(i));
            }
        }
    }
    
}

